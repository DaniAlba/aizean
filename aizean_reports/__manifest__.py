# -*- coding: utf-8 -*-
###############################################################################
#
#    Copyright (C) 2015 Salton Massally (<smassally@idtlabs.sl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

{
    "name": "Aizean Customized reports",
    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'author': "Daniel Alba<dalba@aizean.com>",
    "website": "http://www.aizean.com",
    "category": "reports",
    "summary": "account invoice report",
    "description": "",
    "depends": [
        "professional_templates",
        "account",
        "sale",
    ],
    "data": [
        'views/account_invoice_report_templates.xml',
        'views/header_footer.xml',
        'views/sale_order_report_templates.xml',
    ],
    'installable': True,
}
